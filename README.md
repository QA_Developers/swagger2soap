# SoapUI Project patching

Adding all requirement variables to SoapUI project after importing swagger definition

![Example](example.png)
## Requirements
- Python 3.4 or newest
- xmltodict 

## Install
`git clone https://gitlab.com/QA_Developers/swagger2soap.git`

`pip install xmltodict`

## Usage
1. Create a new project in SoapUI
2. Import swagger definition
3. Save project
4. Run `python swagger2soap_patch.py -i <input project-file> -o <output project-file> -a` 
5. Import patched project in SoapUI