import xmltodict
import pprint
import json
import optparse
from sys import argv, exit

parser = optparse.OptionParser()
parser.add_option("-i", "--input", dest="input_file", help="Input file. SoapUI project in xml format", default="")
parser.add_option("-o", "--output", dest="output_file", help="Output file. SoapUI project in xml format", default="")
parser.add_option("-a", "--auth", dest="auth", help="Use Bearer authorization", action='store_true')
options, args = parser.parse_args()

def createParameter(name):
	return {'@key': name, "@value": "${#Project#%s}" % name}

def createParameterAuthorization(auth_type):
	return {'@key': 'Authorization', "@value": "%s ${#Project#token}" % auth_type}

def  addParameterAuthorization():
	return {   '@required': 'true',
                         'con:description': 'Authorization',
                         'con:name': 'Authorization',
                         'con:style': 'HEADER'}

def createParameters(values):
	parameters = {"con:entry": []}
	[parameters["con:entry"].append(createParameter(name)) for name in values]
	parameters["con:entry"].append(createParameterAuthorization('Bearer'))
	return parameters

def addPropertiesToProject(variabels, endpoint, endpoint_var):
	s = {'con:property' : []}
	[s['con:property'].append({'con:name': name, 'con:value': ''}) for name in variabels]
	#Setup endpoint
	s['con:property'].append({'con:name': endpoint_var, 'con:value': endpoint})
	return s

def addAuthorization(res):
	if type(res["con:method"]) == list:
		for meth in res["con:method"]:
			if meth['con:parameters']:
				if type(meth['con:parameters']['con:parameter']) == dict:
					default = meth['con:parameters']['con:parameter']
					meth['con:parameters']['con:parameter'] = []
					meth['con:parameters']['con:parameter'].append(default)
					meth['con:parameters']['con:parameter'].append(addParameterAuthorization())
				else:
					meth['con:parameters']['con:parameter'].append(addParameterAuthorization())
				# print(type(meth['con:parameters']))
				# dict(list(meth['con:parameters']['con:parameter']).append(addParameterAuthorization()))
			else:
				meth['con:parameters'] = {'con:parameter': addParameterAuthorization()}
	else:
		if res['con:method']['con:parameters']:
			if type(res['con:method']['con:parameters']['con:parameter']) == dict:
				default = res['con:method']['con:parameters']['con:parameter']
				res['con:method']['con:parameters']['con:parameter'] = []
				res['con:method']['con:parameters']['con:parameter'].append(default)
				res['con:method']['con:parameters']['con:parameter'].append(addParameterAuthorization())
			else:
				res['con:method']['con:parameters']['con:parameter'].append(addParameterAuthorization())
		else:
			res['con:method']['con:parameters'] = {'con:parameter': addParameterAuthorization()}
	return res

def interfaceProcessing(interface):
	global input_file
	global output_file
	global props
	global options
	
	pp = pprint.PrettyPrinter(indent=4)
	interface['@basePath'] = ''
	print(interface['@name'])
	for res in interface["con:resource"]:
		res = addAuthorization(res) if options.auth else res
		dots = 50-len(res["@name"]) if len(res["@name"]) < 50 else 90-len(res["@name"])
		endpoint = interface["con:endpoints"]["con:endpoint"]
		endpoint_var = '${#Project#' + endpoint[endpoint.find('//')+2:endpoint.find('.')] + '_url}'
		if type(res["con:method"]) == list:
			print('	[ > ] ' + res['@name'] + '.'*dots + 'patched')
			for meth in res["con:method"]:
				# MORE THEN 1 METHOD
				param_names = []
				if not meth['con:parameters']:
					meth['con:request']["con:endpoint"] = endpoint_var
					continue
				elif type(meth['con:parameters']['con:parameter']) == list:
					# MORE THEN 1 PARAMETER
					[param_names.append(param['con:name']) for param in meth['con:parameters']['con:parameter']]
					meth['con:request']["con:parameters"] = createParameters(param_names)
				elif type(meth['con:parameters']['con:parameter']) == dict:
					#SINGLE PARAMETER
					param_names.append(meth['con:parameters']['con:parameter']['con:name'])
					meth['con:request']["con:parameters"] = createParameters(param_names)
				props += param_names
				meth['con:request']["con:endpoint"] = endpoint_var
		elif type(res["con:method"]) == dict:
				# SINGLE METHOD
				print('	[ > ] ' + res['@name'] + '.'*dots + 'patched')
				param_names = []
				if not res["con:method"]['con:parameters']:
					res["con:method"]['con:request']["con:endpoint"] = endpoint_var
					continue
				elif type(res["con:method"]['con:parameters']['con:parameter']) == list:
					# MORE THEN 1 PARAMETER
					[param_names.append(param['con:name']) for param in res["con:method"]['con:parameters']['con:parameter']]
					res["con:method"]['con:request']["con:parameters"] = createParameters(param_names)
				elif type(res["con:method"]['con:parameters']['con:parameter']) == dict:
					#SINGLE PARAMETER
					param_names.append(res["con:method"]['con:parameters']['con:parameter']['con:name'])
					res["con:method"]['con:request']["con:parameters"] = createParameters(param_names)
				res["con:method"]['con:request']["con:endpoint"] = endpoint_var
	props = list(set(props))
	doc['con:soapui-project']['con:properties'] = addPropertiesToProject(props, endpoint, endpoint_var)
	out_xml = xmltodict.unparse(doc, pretty=False)
	print()
	with open(output_file, 'w') as f:
		f.write(out_xml)


if __name__ == '__main__':
	usage = 'Usage: python swagger2soap_patch.py <input xml-file> <output xml-file>'
	input_file = options.input_file
	output_file = options.output_file
	endpoint_var = ''
	props = []
	if '.xml' not in input_file:
		print('Wrong input file.', usage)
		exit(1)
	elif '.xml' not in output_file:
		print('Wrong output file name. Should be xml.', usage)
		exit(1)
	try:
		with open(input_file, 'r') as fd:
			doc = xmltodict.parse(fd.read())
		doc = json.loads(json.dumps(doc))
		interface = doc['con:soapui-project']["con:interface"]
		if type(interface) == dict:
			interfaceProcessing(interface)
		elif type(interface) == list:
			[interfaceProcessing(interface) for interface in interface]
	except Exception as e:
		print('Error - ' + str(e))
		exit(1)
